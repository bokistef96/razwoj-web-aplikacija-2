import React, { Component } from 'react';
import './App.css';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/pages/Header';
import Footer from './components/pages/Footer';
import Sidebar from './components/pages/Sidebar';
import HomePage from './components/pages/HomePage';
import AddPage from './components/pages/AddPage';
import UpdatePage from './components/pages/UpdatePage';
import UpdateStudentPage from './components/pages/UpdateStudentPage';
import DeletePage from './components/pages/DeletePage';
import SearchPage from './components/pages/SearchPage';

class App extends Component {
  render() {
    return (

      <div className="App">
          <Header></Header>
          <Router>
              <div className="container-fluid">
                  <div className="row">

                      <Sidebar></Sidebar>
                    
                      <Switch> {/*mora ovakvim redosledom*/}
                          <Route path="/izmeni" component={UpdatePage}/>
                          <Route path="/pretrazi" component={SearchPage}/>
                          <Route path="/dodaj" component={AddPage}/>
                          <Route path="/obrisi" component={DeletePage}/>
                          <Route path="/izmenaStudenta" exact component={UpdateStudentPage}/>
                          <Route path="/" exact component={HomePage}/>
                         
                      </Switch>

                  </div>
                
              </div>  
          </Router>
          <Footer></Footer>
      </div>
    );
  }
}

export default App;
