import React,{Component} from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {inputSearch,requestData} from '../../store/actions';



class SearchPage extends Component{
    componentDidMount(){
        this.props.requestData();
    }
    render(){
        return(

            <div className="col-lg-10 backgroundColor2"  >              
                <div className="row main" id="bodyPart">
                                    
                    <div className="other" id="other">

                            <label><b>Uneti broj indexa studenta za pretraživanje:</b></label>
                            <input onChange={this.props.inputSearch} id='pretrazi' type='text' className='form-control' placeholder='Broj indexa'/>
                        
                    </div>
                
                    <div className="portletsArray" id="portletsArray">
                    
                        {stampaj(this.props.students)}

                    </div>
                                
                </div>            
            </div>

            )
        
    }
}

function stampaj(students){
        return students.map(student=>{
            return(
    
                 <div className='portleta  ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'>
                        <div className='portlet-headera ui-widget-header ui-corner-all'>{student.ime}  {student.prezime}</div>
                        <div className='portlet-contenta'> <strong>Broj indexa:</strong> {student.brIndexa}<br/><strong> Ime:</strong> {student.ime} <br/> <strong>Prezime:</strong> {student.prezime}<br/> <strong>Broj položenih ispita:</strong> {student.brPolozenihIspita}<br/> <strong>Prosek:</strong> {student.prosek}</div>
                        <div className="dugme" id="dugmeDiv"> </div>
                 </div>
                
            )
    
        })
    
}

function mapStateToProps(state) {
    return {

        students: state.students,
        
    }
}

function mapDispatchToProps(dispatch)
{ 
    return bindActionCreators({inputSearch,requestData }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps) (SearchPage);