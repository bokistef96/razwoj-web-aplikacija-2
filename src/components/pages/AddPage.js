import React,{Component} from 'react';
import '../../App.css';
import {addData} from '../../store/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {rangeValue,setDefault} from '../../store/actions';

class AddPage extends Component{

    render(){
    
        let title=document.getElementById("nazivModal");
        title.innerHTML="Dodavanje studenta";
        let body=document.getElementById("bodyModal");
        body.innerHTML="Uspesno ste dodali novog studenta";
    
        return (

            <div className="col-lg-10 backgroundColor2"  >              
                <div className="row main" id="bodyPart">
                                    
                    <div className="other" id="other">
                        <div id='addF'>
                                <label><b>Uneti podatke za dodavanje novog studenta:</b> </label>
                                <input  type='text' className='form-control inputUpdate aaa' placeholder='Ime' id='imeA' />
                                <input id='prezimeA' type='text' className='form-control inputUpdate' placeholder='Prezime'/>
                                <input id='brIndexaA' type='text' className='form-control inputUpdate' placeholder='Broj indexa'/>
                                <input id='brIspitaA' type='text' className='form-control inputUpdate' placeholder='Broj položenih ispita'/>
                                <input onChange={this.props.rangeValue} id='prosekA' type='range' className='rangeInput'/> <br/>
                                <div className="range">Prosek ocena: {this.props.range} </div>
                                <button onClick={()=>{addData(); brisi(); this.props.setDefault();}} className="btn btn-md inputUpdate" id="dugmeAdd" data-target="#myModal" data-toggle= "modal">Dodaj studenta </button>

                        </div>
                    </div>
                    
                                
                </div>        
            </div>
            
            )
            
        }
}
function brisi(){

    document.getElementById("prezimeA").value="";
    document.getElementById("imeA").value="";
    document.getElementById("brIndexaA").value="";
    document.getElementById("brIspitaA").value="";
    document.getElementById("prosekA").value=50;
    
    
}

function mapStateToProps(state) {
    return {

        students: state.students,
        range: state.range,
        
    }
}

function mapDispatchToProps(dispatch)
{ 
    return bindActionCreators({rangeValue,setDefault }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps) (AddPage);