import React from 'react';
import '../../App.css'



function HomePage(){
    return(
                    
            <div className="col-lg-10 backgroundColor2"  >              
                <div className="row main" id="bodyPart">
                                    
                    <div className="other" id="other">

                        <div id="pocetna">
                            <div className="naslov"><h3>Dobro došli na sajt koji služi za pregled studenata Elektronskog fakulteta u Nišu !</h3> </div>
                            
                            <div className="tekst">
                                <h4> Korisni linkovi povezani sa Elektronskim fakultetom:<br/> </h4>
                                <ul>
                                <li><a href="http://www.elfak.ni.ac.rs">ELFAK</a>-Sajt fakulteta</li>
                                <li><a href="https://sip.elfak.ni.ac.rs/">SIP</a>-Studentski informativni portal</li>
                                <li><a href="https://sicef.info">SICEF</a>-Studentski inovacioni centar Elektronsog fakulteta</li>
                                <li><a href="https://cs.elfak.ni.ac.rs/nastava/">CS</a>-Katedra za računarstvo</li>
                                </ul>
                            
                            </div>
                         
                        </div>
                            
                    </div>
                        
                </div>            
            </div>

        )
    }

export default HomePage;