import React,{Component} from 'react';
import '../../App.css'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {inputSearch,selectedStudent,requestData,setRange} from '../../store/actions';
import { Link} from 'react-router-dom';

class UpdatePage extends Component{
    componentDidMount(){
        this.props.requestData();
    }
    render(){
        let title=document.getElementById("nazivModal");
        title.innerHTML="Izmena";
        let body=document.getElementById("bodyModal");
        body.innerHTML="Uspesno ste izmenili traženog studenta";

        return(
            
            <div className="col-lg-10 backgroundColor2"  >  
                <div className="row main" id="bodyPart">
                                    
                    <div className="other" id="other">
                        <label><b>Uneti broj indexa studenta za izmenu:</b></label>
                        <input onChange={this.props.inputSearch} id='pretrazi' type='text' className='form-control' placeholder='Broj indexa'/>
                        
                    </div>
                    
                    <div className="portletsArray" id="portletsArray">
                        {stampaj(this.props)}
                    </div> 
                                
                </div>            
            </div>

            )
        }
}



function stampaj(props){

    return props.students.map(student=>{
        return(

                <div className='portleta  ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'>
                    <div className='portlet-headera ui-widget-header ui-corner-all'>{student.ime}  {student.prezime}</div>
                    <div className='portlet-contenta'> <strong>Broj indexa:</strong> {student.brIndexa}<br/><strong> Ime:</strong> {student.ime} <br/> <strong>Prezime:</strong> {student.prezime}<br/> <strong>Broj položenih ispita:</strong> {student.brPolozenihIspita}<br/> <strong>Prosek:</strong> {student.prosek}</div>
                    <div className="dugme" id="dugmeDiv">  <button onClick={() =>{selectStudent(student,props);props.setRange(student.prosek);}} className="btn btn-sm" ><Link  to="/izmenaStudenta">Izmeni studenta</Link> </button></div>
                </div>
            
        )

    })

}

function selectStudent(student,props){
    props.selectedStudent(student);

}
function mapStateToProps(state) {
    return {
        students: state.students,
        activeStudent:state.activeStudent
    }
}

function mapDispatchToProps(dispatch){

    return bindActionCreators({inputSearch,selectedStudent,requestData,setRange }, dispatch);
}
        
export default connect(mapStateToProps,mapDispatchToProps) (UpdatePage);