import React from 'react';
import '../../App.css'
import elfak from '../../elfak.jpg'

function Header(){
    return(

            <header className="container-fluid">
                    
            <div className="headerDiv"><img src={elfak} className="headerPicture" alt="ELFAK" /></div>
            <h2> SEF- <small> Studenti Elektronskog fakulteta</small></h2>  
            </header>

        )
    }
export default Header;