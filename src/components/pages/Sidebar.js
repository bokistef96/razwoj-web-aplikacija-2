import React from 'react';
import '../../App.css'
import { NavLink} from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {startValue} from '../../store/actions';

function Sidebar(props){
    
    return(

        <div className="col-lg-2 navbar backgroundColor">
            
            <div className="row content">
                <div className="columnNav">
                
                        <button onClick={props.startValue} className="btn btn-lg btn-block disabled selection buttonNav" id="btnPocetna"><NavLink className="link" exact to="/">Početna strana</NavLink></button>
                
                        <button  onClick={props.startValue} className="btn btn-lg btn-block disabled selection buttonNav" id="btnPretrazi"><NavLink  className="link" to="/pretrazi">Pretraži studente</NavLink></button>
                        
                        <button  onClick={props.startValue} className="btn btn-lg btn-block disabled selection buttonNav" id="btnIzmeni"><NavLink  className="link" to="/izmeni">Izmeni studenta</NavLink></button>
                        
                        <button onClick={props.startValue} className="btn btn-lg btn-block disabled selection buttonNav" id="btnDodaj"><NavLink  className="link" to="/dodaj">Dodaj studenta</NavLink></button>
                        
                        <button onClick={props.startValue}  className="btn btn-lg btn-block disabled selection buttonNav" id="btnObrisi"><NavLink className="link"  to="/obrisi">Obriši studenta</NavLink></button>

                </div>
            </div>
        
        </div>

        )
    }
    
    function mapStateToProps(state) {
        return {

            students: state.students,
            
        }
    }
    function mapDispatchToProps(dispatch)
    { 
        return bindActionCreators({startValue }, dispatch);
    }

export default connect(mapStateToProps,mapDispatchToProps) (Sidebar);