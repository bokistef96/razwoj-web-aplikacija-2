import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {inputSearch,requestData,deleteData} from '../../store/actions';



class DeletePage extends Component{
    componentDidMount(){
        console.log(this.props.students);
        this.props.requestData();
    }
    render(){
    let title=document.getElementById("nazivModal");
    title.innerHTML="Brisanje studenta";
    let body=document.getElementById("bodyModal");
    body.innerHTML="Uspesno ste obrisali traženog studenta";
    
    return(

        <div className="col-lg-10 backgroundColor2"  >              
            <div className="row main" id="bodyPart">
                                
                <div className="other" id="other">
                    <label><b>Uneti broj indexa studenta za brisanje:</b></label>
                    <input onChange={this.props.inputSearch} id='pretrazi' type='text' className='form-control' placeholder='Broj indexa'/>
                </div>
                
                <div className="portletsArray" id="portletsArray">
                    {this.stampaj()}
                </div>
                            
            </div>            
        </div>


        )
    }
     stampaj(){
        return this.props.students.map(student=>{
            return(
    
                 <div className='portleta  ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'>
                        <div className='portlet-headera ui-widget-header ui-corner-all'>{student.ime}  {student.prezime}</div>
                        <div className='portlet-contenta'> <strong>Broj indexa:</strong> {student.brIndexa}<br/><strong> Ime:</strong> {student.ime} <br/> <strong>Prezime:</strong> {student.prezime}<br/> <strong>Broj položenih ispita:</strong> {student.brPolozenihIspita}<br/> <strong>Prosek:</strong> {student.prosek}</div>
                        <div className="dugme" id="dugmeDiv"> <button onClick={()=>{this.props.deleteData(student.id);document.getElementById('pretrazi').value="";}} className="btn btn-sm"data-target="#myModal"data-toggle="modal" >Obriši studenta </button></div>
                 </div>
                
            )
    
        })
    
    }
    selectStudent(student){
        this.props.selectedStudent(student);
    
    }
}


function mapStateToProps(state) {

    return {
        students: state.students,
    }
}

function mapDispatchToProps(dispatch){

    return bindActionCreators({inputSearch,requestData,deleteData}, dispatch);
}
        
export default connect(mapStateToProps,mapDispatchToProps) (DeletePage);