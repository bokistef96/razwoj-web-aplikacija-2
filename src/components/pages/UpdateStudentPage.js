import React,{Component} from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import { Link} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {startValue,updateData,rangeValue} from '../../store/actions';



class UpdateStudentPage extends Component{
    render(){    
        return(

    <div className="col-lg-10 backgroundColor2">              
        <div className="row main" id="bodyPart">
                            
            <div className="other" id="other">

                <label><b/>Uneti podatke za ažuriranje traženog studenta: </label>
                <input id='imeU' type='text' className='inputUpdate form-control' defaultValue={this.props.activeStudent.ime} placeholder='Ime'/>
                <input id='prezimeU' type='text' className='form-control inputUpdate'defaultValue={this.props.activeStudent.prezime}  placeholder='Prezime'/>
                <input id='brIndexaU' type='text' className='form-control inputUpdate' defaultValue={this.props.activeStudent.brIndexa}  placeholder='Broj indexa'/>
                <input id='brIspitaU' type='text' className='form-control inputUpdate' defaultValue={this.props.activeStudent.brPolozenihIspita}  placeholder='Broj položenih ispita'/>
                <input onChange={this.props.rangeValue} id='prosekU' type='range' className='rangeInput'/> <br/>
                <div className="range" >Prosek ocena: {this.props.range} </div>
                <Link className="link" to="/izmeni"><button onClick={()=>{this.props.updateData(this.props.activeStudent.id)}} className='btn btn-sm inputUpdate' id='potvrdiIzmenu' data-target="#myModal"data-toggle="modal">Izmeni</button> </Link>

           </div>     
        </div>            
    </div>

        )
    }
}

function mapStateToProps(state) {
    return {
        activeStudent:state.activeStudent,
        range:state.range
    }
}
function mapDispatchToProps(dispatch)
{ 
    return bindActionCreators({startValue,updateData,rangeValue }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps) (UpdateStudentPage);