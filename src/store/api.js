const url="http://localhost:4001/students/";
export const fetchData= async () => {
    try{
        const response=await fetch (url);
        const data=await response.json();
    return data;
    }
    catch(e){
        console.log("Greska",e);
    }
}
export const addData = async () => {

    try{
        const response= await fetch(url, {
            method: 'post',
            headers: {
                
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
           
            body: JSON.stringify({
                ime: document.getElementById("imeA").value,
                prezime: document.getElementById("prezimeA").value,
                brIndexa: document.getElementById("brIndexaA").value,
                brPolozenihIspita:document.getElementById("brIspitaA").value,
                prosek:(document.getElementById("prosekA").value/20+5),
            })
        })
        console.log("RESPONSE",response);
    }
    catch(e){
        console.log("Greska",e)
    }

}

export const updateData= async (id) => {
    
    try{
        const response= await fetch(`${url}${id}`, {
            method: 'put',
            headers: {
                
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
           
            body: JSON.stringify({
                ime: document.getElementById("imeU").value,
                prezime: document.getElementById("prezimeU").value,
                brIndexa: document.getElementById("brIndexaU").value,
                brPolozenihIspita:document.getElementById("brIspitaU").value,
                prosek:(document.getElementById("prosekU").value/20+5),
            })
          })
        console.log("RESPONSE",response);
    }
    catch(e){
        console.log("Greska",e)
    }
}

export const deleteData= async (id) => {
    
    try{
        const response= await fetch(`${url}${id}`, {
            method: 'delete',
            headers: {
                
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
          })
        console.log("RESPONSE",response);
    }
    catch(e){
        console.log("Greska",e)
    }
}


