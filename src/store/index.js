import {createStore,applyMiddleware} from 'redux';
import {combineReducers} from 'redux';
import {StudentsReducer} from './reducers/students.reducer';
import {ActiveStudent} from './reducers/activeStudent.reducer';
import {RangeReducer} from './reducers/range.reducer';
import createSagaMiddleware from 'redux-saga'
import mySaga from './sagas';

const reducer = combineReducers({
  students: StudentsReducer,
  activeStudent:ActiveStudent,
  range:RangeReducer
})

const sagaMiddleware=createSagaMiddleware();

const store= createStore(reducer,applyMiddleware(sagaMiddleware));

sagaMiddleware.run(mySaga);

export default store;