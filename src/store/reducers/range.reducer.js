import { RANGE,SET_RANGE, SET_DEFAULT } from '../actions';


export function RangeReducer (state=0, action){
    
        switch(action.type){
            
                    case SET_RANGE:{

                        state=action.payload;
                        return state;
                    }    
                    case RANGE:{
                       
                        let value=(action.payload/20)+5; 
                        state=value;            
                        return state;
                        
                    }
                    case SET_DEFAULT:{
                        
                        state=0;
                        return state;
                    }
                    default:
                        return state;
                    
                }
        
    }