import { INPUT_SEARCH, START_VALUE, RECIVE_DATA } from '../actions';

let initialState =[];

export function StudentsReducer (state=[], action){

    switch(action.type){
        
                case INPUT_SEARCH:
                {   
                    state=initialState;
                    return Object.assign([], findSugestions(state,action.payload));                    
                }
                case START_VALUE:
                {
                    state=[];
                    return state;
                    
                }
                case RECIVE_DATA:{

                    initialState=action.payload;
                    return state=[];
                }
                
                default:
                    return state;
                
            }
    
}
function findSugestions(Students,str) {
    
    let pom=[];
    if(str!=="")    
    { 
        pom = Students.filter(element => element.brIndexa.includes(str));
    }
    else pom=[];
    return pom;
}

