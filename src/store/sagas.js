import { call, put, takeLatest } from 'redux-saga/effects';
import {REQUEST_DATA,reciveData, DELETE_DATA,UPDATE_DATA, doneSelect} from './actions';
import {fetchData,deleteData,updateData} from './api';

function* izBaze(action) {
   try {
     const data = yield call(fetchData);
      yield put(reciveData(data));

   } catch (e) {
     console.log("greska u sagas",e)
   }
}

function* brisiIzBaze(action) {
  try {

    yield call (deleteData,action.payload);
    
    const data = yield call(fetchData);
    yield put(reciveData(data));

  } catch (e) {
    console.log("greska u sagas",e)
  }
}

function* izmeniIzBaze(action) {
  try {

    yield call (updateData,action.payload);
    
    const data = yield call(fetchData);
    yield put(reciveData(data));
    yield put(doneSelect());

  } catch (e) {
    console.log("greska u sagas",e)
  }
}


 function* mySaga() {

    yield takeLatest(DELETE_DATA, brisiIzBaze);
    yield takeLatest(UPDATE_DATA, izmeniIzBaze);
    yield takeLatest(REQUEST_DATA, izBaze);
}

export default mySaga;