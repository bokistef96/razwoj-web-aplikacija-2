export const INPUT_SEARCH = 'INPUT_SEARCH';
export const START_VALUE = 'START_VALUE';
export const SELECTED_STUDENT = 'SELECTED_STUDENT';
export const REQUEST_DATA='REQUEST_DATA';
export const RECIVE_DATA='RECIVE_DATA';
export const DELETE_DATA='DELETE_DATA';
export const UPDATE_DATA="UPDATE_DATA";
export const DONE="DONE";
export const RANGE="RANGE";
export const SET_RANGE="SET_RANGE";
export const SET_DEFAULT="SET_DEFAULT";

export function inputSearch(ev){

    return { type:INPUT_SEARCH, payload:ev.target.value};
}
export function startValue(){
    
        return { type:START_VALUE };
}
export function selectedStudent(student){
    
        return { type:SELECTED_STUDENT, payload:student };
}
export function requestData(){
        
        return { type:REQUEST_DATA };
}
export function reciveData(data){
        
        return { type:RECIVE_DATA, payload:data };
}

export function deleteData(data){

        return {type:DELETE_DATA,payload:data};

}
export function updateData(data){

        return {type:UPDATE_DATA,payload:data};
        
}
export function doneSelect(){
        
        return {type:DONE};
}
export function rangeValue(ev){

        return {type:RANGE, payload:ev.target.value}
}
export function setRange(range){

        return {type:SET_RANGE, payload:range}
}
export function setDefault(){
        
        return {type:SET_DEFAULT}
}




